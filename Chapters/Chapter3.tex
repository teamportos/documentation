% Chapter 1

\chapter{Desarrollo} % Main chapter title

\label{Chapter3}

%----------------------------------------------------------------------------------------

\section{Funcionamiento de FreeOSEK}
Mediante la revisión de proyectos anteriores, la creación de nuevos proyectos y el análisis de todos los archivos creados en estos proyectos, fuimos capaces de comprender el funcionamiento de FreeOSEK para luego poder modificar el mismo.
En una primera instancia pudimos distinguir los dos archivos más importantes del proyecto: 
\begin{enumerate}  
\item Un archivo escrito en lenguaje C que explicita qué hace cada tarea y pone al Sistema Operativo en ejecución por medio de la función \textit{\textbf{main()}}. 
\item Un archivo escrito en lenguaje OIL, el cual define la configuración inicial del Sistema, las tareas del proyecto junto a sus características como \textit{Stack} y Prioridad, y demás estructuras como alarmas, contadores y recursos.
\end{enumerate}
El código fuente del Sistema Operativo es generado mediante archivos PHP, que analizan las definiciones en el archivo OIL y generan código en Lenguaje C a la medida.
En el siguiente ejemplo, se muestra un extracto del archivo \verb|OS_Internal_Cfg.c.php|, en el cual se realiza la definición del stack de las tareas de un proyecto:

\begin{mdframed}
\verb|OS_Internal_Cfg.c.php|
\vskip -2mm
\noindent\makebox[\linewidth]{\rule{402px}{0.1pt}}
\begin{lstlisting}
...
$tasks = $this->helper->multicore->getLocalList("/OSEK", 
"TASK");

foreach ($tasks as $task)
{
   print \"/** \brief $task stack */\n";
   print "#if ( x86 == ARCH )\n";
   print "uint8 StackTask" . $task . "[" . $this->config->
   getValue("/OSEK/" . $task, "STACK") ." +
   TASK_STACK_ADDITIONAL_SIZE];\n";
   print "#else\n";
   print "uint8 StackTask" . $task . "[" . $this->config->
   getValue("/OSEK/" . $task, "STACK") ."];\n";
   print "#endif\n";
}
print "\n";
...
\end{lstlisting}
\end{mdframed}

Podemos ver que se utiliza una cláusula \textit{for} para iterar entre las tareas definidas, generando código C para cada una de ellas. Esto permite que no sea necesario escribir este código a mano para cada tarea una y otra vez.
Resumiendo, podemos listar una serie de pasos que reflejan el funcionamiento interno de FreeOSEK:
\begin{enumerate}  
\item En el archivo OIL (por ejemplo, Proyecto1.oil), se definen las tareas que existirán en el proyecto, además de los recursos, alarmas, contadores, entre otras estructuras.
\item En el archivo C (por ejemplo, Proyecto1.c), se define el funcionamiento de cada tarea y se inicia la ejecución del RTOS en la función \textit{\textbf{main()}}.
\item Internamente, mediante archivos PHP se genera código C que reflejan la definición de las tareas y el funcionamiento general del Sistema Operativo.
\item Para poder probar el proyecto, primero hay que asegurarse que el proyecto que se encuentre definido en el archivo \verb|Makefile.mine| sea el que desea ejecutarse.
\item Se debe escribir en la consola (parados dentro de la carpeta de proyecto) el comando \verb|make all| para compilar el proyecto.
\item Finalmente, para descargar el proyecto a la placa de desarrollo, se debe ejecutar el comando \verb|make download|. 

Para más detalles sobre el funcionamiento de FreeOSEK ver el documento \textit{"Breve introducción a OSEK-VDX"} del Ingeniero Mariano Cerdeiro.
\end{enumerate}



%----------------------------------------------------------------------------------------

\section{Estructuras de Datos}
Luego de comprender la estructura y el funcionamiento básico del RTOS, procedimos con el análisis de las Estructuras de Datos. Esto es, observar cómo y dónde se almacenan las tareas y el resto de estructuras como alarmas y recursos.

Se puede encontrar un vector en el cual se hallan todos los descriptores de tareas, que inicialmente estaba en memoria \textit{Flash}. Como se explicó previamente, un descriptor de tareas contiene los datos escenciales de una tarea, tales como el \textit{TaskID}, el \textit{Entry Point}, la prioridad, el \textit{stack}, entre otros. De esta manera, al crear una tarea, todos estos datos se almacenan en una posición del vector, que no es otra cosa que el \textit{TaskID} (Es decir, FreeOSEK define al \textit{TaskID} de una determinada tarea como la posición en la cual se encuentra su descriptor: Si el descriptor de una tarea está en la primera posición del vector, su \textit{TaskID} será 0). Para ejecutar una tarea, se recibe el \textit{TaskID}, se accede a la posición del vector señalada por el mismo para poner la tarea en la cola \textit{Ready}, con el fin de poder comenzar su ejecución cuando sea posible. Al final de la tarea, es necesaria la llamada al sistema \verb|TerminateTask()|, que quita la tarea de la cola \textit{Ready}.

Luego, pudimos observar que las alarmas presentaban una estructura similar: En memoria también existe un vector de alarmas en el cual se guardan los datos básicos de cada alarma (Un \textit{ID}, las tareas que la utilizan, información del contador, etc.). Para los recursos existe una estructura de datos binarios en la que cada bit representa un recurso, por lo cual en cada tarea se presenta una máscara que indica cuáles recursos no utiliza la tarea con un 0, y cuáles si, con un 1. El funcionamiento de los eventos es similar.

Teniendo ya una idea sobre cómo funciona el RTOS y cómo se guardan las estructuras de datos, pasamos a enfocarnos en cómo hacer que estas estructuras presenten un comportamiento dinámico. 

\section{Comportamiento Dinámico}

Al encontrarse todas las Estructuras de Datos alojadas en Memoria \textit{Flash}, para darles comportamiento dinámico lo primero que tuvimos que hacer fue pasarlas a Memoria \textit{RAM} (completamente necesario ya que la memoria \textit{RAM} es la memoria volátil del dispositivo). A partir de ahí, tuvimos que modificar estas estructuras para que su comportamiento sea adecuado para trabajar con tareas dinámicas.

Como se explicó en la sección anterior, existe un vector de tareas que posee los descriptores. Una parte de la generación de este vector se mostró en la sección 2 de este capítulo, en el que vimos que un archivo en lenguaje PHP toma los datos del archivo OIL y genera código C a partir de él. Esto significa que para cada tarea estática (definida en el OIL) se reserva un espacio de memoria fijo, por lo que no hay posibilidad de ocupar más memoria en tiempo de ejecución que la asignada en tiempo de compilación. Esto lleva a una de las grandes ventajas del Estándar OSEK que es la imposibilidad de agotamiento de memoria en tiempo de ejecución, pero a la vez significa una complicación para implementar tareas dinámicas, ya que las tareas creadas en tiempo de ejecución no se pueden agregar al vector de tareas debido a que el tamaño del mismo ya fue establecido desde un principio.

La solución que decidimos implementar para poder agregar tareas dinámicas fue la siguiente:

En el archivo OIL se definirá una nueva estructura, llamada \verb|DYNAMIC|, que contará con los datos necesarios para la definición de tareas dinámicas. En esta estructura es necesario definir:

\begin{enumerate}
\item El número máximo de Tareas Dinámicas (\verb|MAX|)
\item La prioridad de \textbf{todas} las Tareas Dinámicas (\verb|PRIORITY|)
\item El tamaño del \textit{stack} que tendrá cada tarea dinámica, pero que será el mismo valor para todas
\item El número máximo de activaciones (\verb|ACTIVATION|)
\item \textit{(Opcional)} Los recursos que utilizarán las Tareas Dinámicas (\verb|RESOURCE|)
\item \textit{(Opcional)} Los eventos que utilizarán las Tareas Dinámicas (\verb|EVENT|)
\end{enumerate}

Como se puede ver, las propiedades de las tareas (como la prioridad, el \textit{stack}, los recursos, etc) son definidas por igual para \textbf{todas} las tareas dinámicas, mientras que para las estáticas pueden ser valores distintos ya que se definen para cada una de ellas. 

Esto podría considerarse una desventaja de nuestra implementación, pero considerando que nuestro enfoque es proveer la capacidad de generar muchas tareas con comportamiento similar, que puedan ser usadas para un sistema que debe atender muchos clientes similares, no creemos que sea una mayor complicación.

De esta manera, llegamos al concepto principal de nuestra implementación: Al definir en la estructura de Tareas Dinámicas el campo \verb|MAX|, podemos conocer cuál será el máximo de memoria que puede ser ocupado (Que, en este caso, será la suma de lo que ocupe cada una de las tareas estáticas, más lo que ocupe cada tarea dinámica multiplicado por la cantidad máxima de las mismas).

Así es como tomamos la decisión de incrementar el tamaño del vector de tareas, para que en la primera parte del mismo se almacenen las tareas estáticas, y en la segunda se reserve un espacio de memoria suficiente para almacenar la cantidad máxima de tareas dinámicas definidas. Al crearse una tarea dinámica, se agrega su descriptor en el primer espacio libre encontrado en el vector. 

Esta decisión fue tomada siguiendo uno de nuestros objetivos del proyecto: Mantener compatibilidad con el estándar OSEK y sus propiedades. Aplicando nuestra decisión de diseño, nos aseguramos que no habrá agotamiento de memoria en tiempo de ejecución.


\subsection{Alarmas Dinámicas}
\subsubsection{Funcionamiento de las alarmas}
El funcionamiento de las alarmas en FreeOSEK, está directamente ligado a los contadores. Por lo tanto, para explicar cómo funciona una alarma, primero debemos explicar cómo funciona un contador.

Los contadores se definen en el archivo OIL y no hay interfaces para acceder a ellos desde la ejecución del programa. Se pueden definir de 2 tipos: Contadores de \textit{Hardware} o de \textit{Software}. El primero utiliza contadores propios del microcontrolador en el que se encuentre funcionando para ejecutar interrupciones periódicas donde se decrementa el valor del contador. En el caso de la CIAA, se utiliza el \textit{Systick} para disparar el decremento de un contador. Una vez que el contador llega a cero, se dispara la acción que la alarma determinada tenía configurada. 

Las alarmas pueden ejecutar 4 tipos de acciones:
\begin{itemize}
\item \verb|ACTIVATETASK|: Activa la tarea, definida para esta alarma en el archivo OIL.
\item \verb|ALARMCALLBACK|: Llama a una función, definida para esta alarma en el archivo OIL.
\item \verb|SETEVENT|: Setea la activación de un evento, definido para esta alarma en el archivo OIL.
\item \verb|INCREMENT|: Incrementa el valor de un contador por \textit{software}, definido para esta alarma en el archivo OIL.
\end{itemize}

\subparagraph{Mecanismo de disparo:}
La función encargada de incrementar el contador de la alarma tiene un nombre algo engañoso, porque en realidad lo que hace es decrementar su valor.

La alarma conserva el valor de cuánto falta para su siguiente ejecución, cada vez que se llama a la función \verb|IncrementAlarm|, en vez de incrementar, se decrementa el tiempo que falta para la ejecución.

Una vez que el tiempo que falta para la ejecución es menor al valor del incremento, se ejecuta la acción definida para la alarma.

\subparagraph{Implementación de alarmas dinámicas:}
Una alarma dinámica es, en esencia, una alarma cuya acción es del tipo \verb|ACTIVATETASK| y desea que la tarea que sea activada sea una tarea dinámica.

Para agregar una alarma dinámica, se extendió el funcionamiento de la acción. Para ello, se decidió adoptar la siguiente sintaxis en los archivos OIL, para indicar que una alarma es dinámica.

\begin{mdframed}
\verb|Alarma dinamica OIL|
\vskip -2mm
\noindent\makebox[\linewidth]{\rule{375px}{0.1pt}}
\begin{lstlisting}
ALARM NombreAlarma {
   COUNTER = NombreContador;
   ACTION = ACTIVATETASK {
      TASK = DYNAMIC;
   };
}
\end{lstlisting}
\end{mdframed}

Gracias a esto, cuando la alarma desea activar la tarea \verb|DYNAMIC|, se da cuenta que debe buscarla en la sección dinámica del vector de tareas.

Para poder lograr esta implementación, se tuvo que agregar una alarma al vector de alarmas por cada tarea dinámica. Esto quiere decir, que el vector de alarmas también tiene una sección dinámica, donde se almacenan todas las alarmas que llaman a tareas dinámicas.

Por cada alarma dinámica definida se crean \textit{n} entradas en la sección dinámica del vector de alarmas, donde cada entrada nueva corresponde a una alarma que activa a una tarea dinámica, siendo \textit{n} igual al numero máximo de tareas dinámicas. Como se necesita saber el id de una alarma para poder actuar sobre ella, y este id es la posición en el vector de alarmas, el programador no puede conocerlo de antemano. Por lo que decidimos crear una macro que ayuda a identificar la alarma asociada a una tarea dinámica.
 

\begin{mdframed}
\verb|Os_Cfg.h.php: GetDynamicAlarm|
\vskip -2mm
\noindent\makebox[\linewidth]{\rule{375px}{0.1pt}}
\begin{lstlisting}
...
if ($dyncount > 0 && count($dynalarms) > 0)
{
  print "#define GetDynamicAlarm(DynAlarm, DynTaskId) 
  	((DynAlarm) + ((DynTaskId) - $task_count))\n";
}
...
\end{lstlisting}
\end{mdframed}

Se puede apreciar que esta macro sólo se define si hay tareas y alarmas dinámicas. Recibe el identificador de una alarma, el \textit{ID} de una tarea dinámica y retorna el \textit{ID} de la alarma dinámica asociada a esa tarea. Para hacerlo, aprovecha que las alarmas se definen en un vector, y que la posición en el vector de la primera alarma dinámica, se asocia al nombre definido en el OIL, mediante una directiva \verb|#define|. Luego le suma el valor que corresponde a la tarea dinámica que se calcula como: \verb|Id Tarea Dinámica - Total de tareas estáticas| y eso resulta ser la posición en el vector de la alarma asociada a la tarea dinámica dada.

Por ejemplo, partiendo del ejemplo del OIL anterior, se podría usar la macro de la siguiente manera:

\begin{mdframed}
\verb|GetDynamicAlarm|
\vskip -2mm
\noindent\makebox[\linewidth]{\rule{375px}{0.1pt}}
\begin{lstlisting}
AlarmType Alarma1 = GetDynamicAlarm(NombreAlarma, TareaDinamica1);
\end{lstlisting}
\end{mdframed}

Luego, se puede utilizar \verb|Alarma1| como parámetro en cualquiera de las primitivas de OSEK para gestión de alarmas.

Cabe mencionar que también exiten los contadores de \textit{software}: Éstos se implementan por medio de una alarma, cuya acción es incrementar el valor del contador. Por ejemplo, para definir un contador por \textit{software} se necesitan un contador por \textit{hardware} y una alarma, cuando se dispare la alarma, la acción definida debe ser \verb|INCREMENT COUNTER| de manera que se incremente el valor de la cuenta del contador por \textit{software}.

\subsection{Recursos y Eventos Dinámicos}
Los recursos se identifican como bits seteados en 1 dentro de un entero de 32 bits, esto quiere decir que se pueden tener hasta 32 recursos distintos en una aplicación.

En este sentido, la implementación del uso de recursos para tareas dinámicas es directa, partiendo de la implementación de tareas estáticas porque sólo se necesita armar el entero de 32 bits con las máscaras de recursos que correspondan a las tareas dinámicas.

Los únicos controles extra ocurren en las primitivas \verb|GetResource| y \verb|ReleaseResource|, éstas que deben ser modificadas para que tengan en cuenta que una tarea puede estar en la sección dinámica del vector.

Los eventos tienen una implementación similar a la de los recursos, por lo que su implementación se deduce directamente.

\section{Nuevas Llamadas al Sistema}
FreeOSEK posee 26 llamadas al sistema. Sin embargo, al añadir funcionalidades dinámicas fue necesario definir cuatro llamadas más:
\begin{itemize}
\item \textbf{NewTask:} Debido a que las tareas dinámicas se crearán en tiempo de ejecución, es necesario que la creación de esta tarea se encuentre dentro del código a ejecutarse. Para esto, se creó la llamada al sistema \verb|NewTask| (Nueva Tarea), que creará una tarea a partir de una función en C en el estado \textit{Suspended}. 

El proceso de creación de una tarea dinámica es el siguiente:
\begin{enumerate}
\item Se define una nueva función en el archivo en lenguaje C del proyecto, cuyo contenido es la serie de pasos que se desea que realice la tarea a crearse.
\item Se debe crear una variable de tipo \textbf{TaskType} (definido como un entero de 8 bits sin signo). Este tipo de dato existía previamente en FreeOSEK y fue creado para alojar el TaskID de una tarea. Al crear una variable de este tipo, estamos creando un TaskID.
\item Debe existir al menos una tarea estática en el proyecto capaz de crear la tarea dinámica (Generalmente todos los proyectos poseen una tarea estática de configuración, que se encarga de definir los parámetros iniciales como el tiempo de activación de las alarmas, la inicialización de \textit{LEDs} o \textit{switches}, etc.). En esta tarea estática se realizará la llamada al sistema, de la forma:

\verb|t = NewTask(funcion, parametro)|

Donde \textit{t} es el TaskID de tipo TaskType creado en el paso 2, \textit{funcion} es el nombre de la función creada en el paso 1, y \textit{parametro} es un valor que se le puede pasar a la función, de tal manera que en la ejecución de tareas si se quiere realizar un trabajo distinto de acuerdo a un dato recibido, sea posible (es decir, esto permite que las tareas dinámicas se comporten tal cual una función parametrizada en lenguaje C). Si no se desea que la tarea sea parametrizada, se debe pasar \verb|NULL| en lugar del parámetro.
\end{enumerate}
De esta manera, lo que hace esta llamada al sistema es crear un \textit{TaskID} de una tarea nueva, y definir su \textit{Entry Point} como el inicio de la ejecución de una función previamente definida. Luego, se almacena el descriptor de la tarea en el vector.

\textbf{Ejemplo: }

\begin{mdframed}
\verb|blinking.c|
\vskip -2mm
\noindent\makebox[\linewidth]{\rule{375px}{0.1pt}}
\begin{lstlisting}
/* 
   Variables que almacenan los identificadores 
   de tareas dinamicas.
*/
TaskType t1;

/* Funciones para prender y apagar LED's */
void funcion(void) {
  Led_Toggle(RGB_B_LED);
  TerminateTask();
}

TASK(Configuracion) {

   /* Inicializacion de los LED's */
   Init_Leds();
   
   /* Creacion de las nuevas tareas */
   t1 = NewTask(funcion, NULL);

   
   /* 
      Arranque de la alarma para la activacion periodica 
      de la tarea dinamica.
   */
   SetRelAlarm(GetDynamicAlarm(AlarmaDinamica, TaskLed1),   0, 1000);

   /* Terminacion de la tarea */
   TerminateTask();
}
\end{lstlisting}
\end{mdframed}


\item \textbf{DestroyTask:} Con esta llamada al sistema se permite la destrucción de tareas dinámicas, algo totalmente necesario para dejar libre un espacio en el vector de tareas para poder almacenar una nueva tarea dinámica.
El formato de la llamada al sistema es:

\verb|DestroyTask()|

No se reciben parámetros debido a que sólo se puede destruir una tarea si esta llamada al sistema se produce durante la ejecución de dicha tarea, es decir, no se acepta la destrucción de una tarea por medio de otras tareas.
Mediante esta llamada al sistema, se accede al vector de tareas en la posición determinada por el \textit{TaskID}, y se setea el \textit{Entry Point} en \verb|NULL|. Esto elimina la referencia a la ejecución de la función definida como el trabajo que realizará la tarea dinámica, por lo cual queda un espacio libre en el vector de tareas listo para ser ocupado por una nueva tarea creada dinámicamente.
\item \textbf{DecreasePriority: } Al haber definido en el archivo OIL una estructura para tareas dinámicas en la cual se especifica la prioridad, implícitamente se está condicionando que todas las tareas dinámicas tengan la misma prioridad (lo cual como se explicó antes, no consideramos una desventaja muy grande), y que siempre será la misma prioridad, es decir, no se puede cambiar. 

Sin embargo, el sistema operativo FreeRTOS sí permite cambiar la prioridad de sus tareas dinámicas, teniendo una considerable ventaja sobre FreeOSEK. Es por esto que buscamos definir una llamada al sistema que permita cambiar la prioridad de todas las tareas dinámicas. 

Ante esto es necesario aclarar que uno de nuestros principales objetivos es el de mantener la compatibilidad con el estándar OSEK, por lo cual la solución que planteemos no puede generar problemas que antes FreeOSEK no tenía. Particularmente en este caso, el problema se presenta en el uso de recursos y el hecho de que el estándar OSEK asegura que no habrán \textit{deadlocks}. 

FreeOSEK asegura que no se producirán \textit{deadlocks} debido a que se asigna al recurso a compartirse una prioridad mayor que la máxima prioridad de todas las tareas que lo utilizan, por lo que si dinámicamente cambiamos la prioridad de una tarea, ésta podría superar la prioridad del recurso (que se asigna en tiempo de compilación y es técnicamente imposible de modificar dinámicamente), lo cual podría generar \textit{deadlocks}.


Por esto, decidimos que esta llamada al sistema sea capaz sólo de \textbf{disminuir} la prioridad de \textbf{una} de las tareas dinámicas. De esta manera, como la prioridad del recurso en cuestión es más alta que la de las tareas que lo utilizan, si se decrementa la prioridad de alguna de estas tareas, ésta seguirá siendo inferior a la prioridad del recurso. 

La sintaxis de la llamada al sistema es la siguiente:

\verb|DecreasePriority(t, cantidad)|

Donde \textit{t} vuelve a ser la variable tipo TaskType que almacena el TaskID de la tarea dinámica, y \textit{cantidad} es el número que se le quiere restar a la prioridad original de las tareas. 

Mediante esta llamada al sistema, no sólo permitimos cambiar la prioridad de las tareas dinámicas, sino que ahora sí es posible tener tareas dinámicas con prioridades distintas, ya que se puede definir en la estructura de tareas dinámicas en el archivo OIL que todas tengan una prioridad alta (por ejemplo, 6), pero en la tarea estática de configuración se puede disminuir la prioridad de cada una de las tareas dinámicas restando distintos valores a cada una.
\item \textbf{GetDynamicParam:} Esta primitiva permite que una tarea obtenga los parámetros que le fueron pasados en el \verb|NewTask|. Si la tarea es dinámica y tiene parámetros, entonces retorna un puntero a los parámetros, caso contrario retorna \verb|NULL|.

Su funcionamiento es muy simple. Cuando una tarea dinámica es creada, con \verb|NewTask|, y se le pasa un parámetro, este parámetro se almacena en un vector de parámetros, en la posición \verb|i = TaskID - (Cant. Tareas Estáticas)|. Cuando una tarea dinámica llama a esta función, se indexa el vector y se busca el parámetro correspondiente.

\end{itemize}
%----------------------------------------------------------------------------------------